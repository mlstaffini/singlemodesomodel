echo "Here's what these scripts do:"
echo "copy-stuff.sh: run this in a folder full of sweep folders to extract only the sweep files from each to a separate folder"
echo "get-borders.py: you put this in the folder created by copy-stuff, python script"
echo "run-get-borders.sh: runs get-borders for every sweep file and makes a file with all the boundaries"

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 24 20:07:51 2021

@author: mls
"""

#import numpy
import sys

filename=str(sys.argv[1])

# file=open("Sweep-OmegaTil_1.500_to_1.500-P_0.540_to_0.540.dat",'r')
file=open(filename,'r')

print(filename, "opened")

data=[];

SRcutoff=0.2;

# loading up the data in a list of floats
for line in file:
    line=line.strip()
    if line and not line[0]=="#":
        vars=line.split()
        # load up the variables in python readable
        pump=float(vars[0].replace('D', 'E'))
        omtil=float(vars[1].replace('D', 'E'))
        light=float(vars[2].replace('D', 'E'))
        data.append([pump,omtil,light])

# initialise bounds
bound1=0
bound2=0
prebool=0 # use to toggle whether we're coming up or down

# this will get the last point that is not SR going up,
# and the first point that is not SR going down. 
# IF WANT TO DO FIRST SR AND LAST SR: change annotated lines
for i in range(len(data)):
    if data[i][2]>SRcutoff and prebool==0:
        bound1=data[i-1][0] # if first SR: bound1=data[i]
        prebool=1
    elif data[i][2]<SRcutoff and prebool==1:
        bound2=data[i][0] # if last SR: bound2=data[i-1]
        prebool=0

print("for OmTil=", data[0][1]," the values of P at boundary are:")
print(bound1, bound2)

outstring=str("%0.6f" % data[0][1])+"   "+str("%0.3f" % bound1)+"   "+str("%0.3f" % bound2)

with open("boundaries.txt","a") as outf:
    outf.write(outstring+"\n")
    # if our sweep is too narrow the second boundary will return 0
    # but it isn't the same as having no jump, throw in an error
    if prebool==1: outf.write("# never came down -- boundaries too narrow"+"\n")

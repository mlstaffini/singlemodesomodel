rm -rf boundaries.txt
echo "# P_up is the pump value of the last non SR point during sweep up, P_down of the first non SR point during sweep down" >> boundaries.txt
echo "# OmTil    P_up    P_down" >> boundaries.txt

for i in *.dat; do ./get-borders.py $i; done
